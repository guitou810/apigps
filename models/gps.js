const mongoose = require('mongoose');

const gpsSchema = mongoose.Schema({
    coordinates: [Number,Number],
    record_timestamp: { type: String, required: true },
});

module.exports = mongoose.model('gps', gpsSchema);