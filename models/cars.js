const mongoose = require('mongoose');

const carsSchema = mongoose.Schema({
    id: Number,
    entreprise: { type: String, required: true },
    imatriculation: { type: String, required: true },
    photo: { type: String, required: true },
});

module.exports = mongoose.model('cars', carsSchema);