const express = require('express');
const moment = require('moment');
const app = express();
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const gps = require('./models/gps');
const cars = require('./models/cars');
const users = require('./models/users');
mongoose.connect('mongodb+srv://root:Guil.120698@cluster0.v8xlt.mongodb.net/Api?retryWrites=true&w=majority', {
    useNewUrlParser: true,
    useUnifiedTopology: true
  })
  .then(() => console.log('Connexion à MongoDB réussie !'))
  .catch(() => console.log('Connexion à MongoDB échouée !'));
app.use(express.json());
app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content, Accept, Content-Type, Authorization');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
  next();
});

// Liste des coordonnées gps
app.get('/api/gps', (req, res, next) => {
    gps.find()
      .then(things => res.status(200).json(things))
      .catch(error => res.status(400).json({
        error
      }));
});

// Ajout d'une coordonnées gps
app.post('/api/gps', (req, res, next) => {
    const d = req.body.record_timestamp
    req.body.record_timestamp = new Date(d).valueOf()/1000
    const gpsData = new gps({
      ...req.body
    });
    console.log(gpsData)
    gpsData.save()
      .then(() => res.status(201).json({
        message: 'Coordonnées enregistré !'
      }))
      .catch(error => res.status(400).json({
        error
      }));
});

// récupération des coordonnées d'un jour
app.get('/api/gps/:dt', (req, res, next) => {
    const date = moment.unix(req.params.dt).toDate()
    date.setHours(2,1,0,0)
    const startDate = (new Date(date)/1000).toFixed(0)

    date.setHours(25,59,59,999);
    const endDate = (new Date(date)/1000).toFixed(0)
    gps.find({record_timestamp:{$gt:startDate, $lt: endDate}  })
      .then(things => {
          res.status(200).json(things)})
      .catch(error => res.status(400).json({
        error
      }));
});

//récupération des dernières coordonnées enregistrées
app.get('/api/gpsOne', (req, res, next) => {
  gps.find().sort({_id:-1}).limit(1)
    .then(things => res.status(200).json(things))
    .catch(error => res.status(400).json({
      error
    }));
});

// liste de tous les utilisateurs
app.get('/api/users', (req, res, next) => {
  users.find()
    .then(things => res.status(200).json(things))
    .catch(error => res.status(400).json({
      error
    }));
});

// liste de tous les véhicules
app.get('/api/cars', (req, res, next) => {
  cars.find()
    .then(things => res.status(200).json(things))
    .catch(error => res.status(400).json({
      error
    }));
});

// liste des véhicules en fonction du nom de l'entreprise
app.get('/api/cars/:dt', (req, res, next) => {
  cars.find({entreprise: req.params.dt })
    .then(things => {
        res.status(200).json(things)})
    .catch(error => res.status(400).json({
      error
    }));
});

  module.exports = app;
