// Dependencies
const fs = require('fs');
const http = require('http');
const https = require('https');
const express = require('express');
const app = require('./app');
/* const dotenv = require('dotenv');
dotenv.config();
console.log("Env: " +  process.env.ENV) */
// Certificate
let credentials = {};
// if (process.env.ENV == 'server') {
//         const privateKey = fs.readFileSync('/etc/letsencrypt/live/chienmix.ml/privkey.pem', 'utf8');
//         const certificate = fs.readFileSync('/etc/letsencrypt/live/chienmix.ml/cert.pem', 'utf8');
//         const ca = fs.readFileSync('/etc/letsencrypt/live/chienmix.ml/chain.pem', 'utf8');

//         credentials = {
//                 key: privateKey,
//                 cert: certificate,
//                 ca: ca
//         };
// }

// Starting both http & https servers
const httpServer = http.createServer(app);
//const httpsServer = https.createServer(credentials, app);

httpServer.listen(3000, () => {
        console.log('HTTP Server running on port 3000');
});

// if (process.env.ENV == 'server') {
//         httpsServer.listen(3001, () => {
//                 console.log('HTTPS Server running on port 443');
//         });
// }